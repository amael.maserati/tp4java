public class ExecutablePoudlard {
    public static void main(String [] args){
        Ecole poudlard = new Ecole("Poudlard");
        Maison serpentar = new Maison("Serpentar");
        Maison griffondor = new Maison("Griffondor");
        Maison serdaigle = new Maison("Serdaigle");
        Maison poufsouffle = new Maison("Poufsouffle");

        Sorcier adrian = new Sorcier("Adrian", 9, 7);
        Sorcier hermione = new Sorcier("Hermione",8,6);
        Sorcier luna = new Sorcier("Luna",2,9);
        Sorcier drago = new Sorcier("Drago",6,5);
        Sorcier norbert = new Sorcier("Norbert",3,7);
        Sorcier neuville = new Sorcier("Neuville",10,4);
        Sorcier pansy = new Sorcier("Pansy",4,10);
        Sorcier gregory = new Sorcier("Gregory",6,7);
        Sorcier gilderoy = new Sorcier("Gilderoy",7,9);
        Sorcier dean = new Sorcier("Dean",9,4);

        serpentar.ajouter("Adrian", 9, 7);
        serpentar.ajouter("Drago",6,5);
        serpentar.ajouter("Pansy", 4, 10);
        serpentar.ajouter("Gregory", 6, 7);

        griffondor.ajouter("Hermione", 8, 6);
        griffondor.ajouter("Neuville", 10, 4);
        griffondor.ajouter("Dean", 9, 4);

        poufsouffle.ajouter("Norbert", 3, 7);

        serdaigle.ajouter("Luna", 2, 9);
        serdaigle.ajouter("Gilderoy", 7, 9);
    }
}
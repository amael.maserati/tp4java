import java.util.*;

public class Maison {
    private String nom;
    private List<Sorcier> sorciers;
    
    public Maison(String nom) {
        this.nom = nom;
        this.sorciers = new ArrayList<>();
    }
    public boolean ajouter(String nomSorcier, int courage, int sagesse){
        return this.sorciers.add(new Sorcier(nomSorcier, courage, sagesse));
    }
    public int nombreEleve(){
        return this.sorciers.size();
    }
    public boolean contientCourageux(){
        boolean contientCourageux = false;
        for (Sorcier sorcier : this.sorciers){
            if (sorcier.estCourageux())
                contientCourageux = true;
        }
        return contientCourageux;
    }
    public Sorcier leMoinsCourageux(){
        Sorcier leMoinsCourageux = this.sorciers.get(0);
        for (Sorcier sorcier : this.sorciers){
            if (sorcier.getCourage() < leMoinsCourageux.getCourage()){
                leMoinsCourageux = sorcier;
            }
        }
        return leMoinsCourageux;
    }
    public Sorcier lePlusSage(){
        Sorcier lePlusSage = this.sorciers.get(0);
        for (Sorcier sorcier : this.sorciers){
            if (sorcier.getSagesse() > lePlusSage.getSagesse()){
                lePlusSage = sorcier;
            }
        }
        return lePlusSage;
    }
    public void trierParCourage(){
        Collections.sort(this.sorciers);
    } 
}
